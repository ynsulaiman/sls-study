const dynamoose = require('./connector-dynamodb');
const schema = require('./schema');

const repository = dynamoose.model('Schema', schema);

repository.save = (data, callback) => {
  const repoInstance = new repository(data);
  repoInstance.save(data, (saveErr) => {
    callback(saveErr);
  });
};

module.exports = repository;
