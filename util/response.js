const log = require('./logging');

const successResponse = (data) => {
    log.info('Output: ', data);

    return {
        statusCode: 200,
        body: JSON.stringify({
            data: data || null
        })
    };
};

const errorResponse = (code, err) => {
    log.error('Error: ', err);

    return {
        statusCode: code || 400,
        body: JSON.stringify({
            err
        })
    };
};

module.exports = {
    successResponse,
    errorResponse
};
