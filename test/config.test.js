const config = require('../config');
const chai = require('chai');

const { expect } = chai;
describe('config', () => {
    it('Should return "dev config"', () => {
        const dev = config.dev();
        expect(dev.logging).to.equal(JSON.stringify({
            enabled: true,
            level: 'debug'
        }));
    });

    it('Should return "staging config"', () => {
        const staging = config.staging();
        expect(staging.logging).to.equal(JSON.stringify({
            enabled: true,
            level: 'debug'
        }));
    });

    it('Should return "demo config"', () => {
        const demo = config.demo();
        expect(demo.logging).to.equal(JSON.stringify({
            enabled: true,
            level: 'debug'
        }));
    });

    it('Should return "prod config"', () => {
        const prod = config.prod();
        expect(prod.logging).to.equal(JSON.stringify({
            enabled: true,
            level: 'info'
        }));
    });
});

