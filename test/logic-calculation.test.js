const logic = require('../logic/calculation');
const logUtils = require('../util/logging');

const chai = require('chai');
const sinon = require('sinon');

const { expect } = chai;

describe('logic/calculation', () => {
    let info;
    let error;
    let debug;

    describe('calculationAdd', () => {
        beforeEach(() => {
            // Skip any log output
            info = sinon.stub(logUtils, 'info');
            error = sinon.stub(logUtils, 'error');
            debug = sinon.stub(logUtils, 'debug');
        });

        afterEach(() => {
            info.restore();
            error.restore();
            debug.restore();
        });


        it('Should return 2 when input are 1 and 1.', () => {
            expect(logic.calculationAdd(1, 1)).to.equal(2);
        });
    });

    describe('calculationDeduct', () => {
        beforeEach(() => {
            // Skip any log output
            info = sinon.stub(logUtils, 'info');
            error = sinon.stub(logUtils, 'error');
            debug = sinon.stub(logUtils, 'debug');
        });

        afterEach(() => {
            info.restore();
            error.restore();
            debug.restore();
        });


        it('Should return 0 when input are 1 and 1.', () => {
            expect(logic.calculationDeduct(1, 1)).to.equal(0);
        });
    });
});

