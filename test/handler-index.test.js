const handler = require('../handler/index');
const validation = require('../validation/index');
const logUtils = require('../util/logging');

const chai = require('chai');
const sinon = require('sinon');
const sinonChai = require('sinon-chai');

chai.should();
chai.use(sinonChai);

describe('handler/index', () => {
    let info;
    let error;
    let debug;

    beforeEach(() => {
        // Skip any log output
        info = sinon.stub(logUtils, 'info');
        error = sinon.stub(logUtils, 'error');
        debug = sinon.stub(logUtils, 'debug');
    });

    afterEach(() => {
        info.restore();
        error.restore();
        debug.restore();
    });

    it('calculationAdd. Validation passed.', (done) => {
        const testData = { valA: 1, valB: 2 };
        const callback = sinon.spy();

        handler.calculationAdd(testData, '', callback);

        validation
            .validate(testData, '/Calculation')
            .then(() => {
                callback.should.have.been.calledWith(null, {
                    statusCode: 200,
                    body: JSON.stringify({
                        data: {
                            result: 3
                        }
                    })
                });

                done();
            })
            .catch(() => {
                done();
            });
    });

    it('calculationAdd. Validation failed.', (done) => {
        const testData = {};
        const callback = sinon.spy();

        handler.calculationAdd(testData, '', callback);

        validation
            .validate(testData, '/Calculation')
            .then(() => {
                done();
            })
            .catch((err) => {
                callback.should.have.been.calledWith(null, {
                    statusCode: 400,
                    body: JSON.stringify({
                        err
                    })
                });

                done();
            });
    });

    it('calculationDeduct. Validation passed.', (done) => {
        const testData = {
            queryStringParameters: {
                valA: 2,
                valB: 1
            }
        };
        const callback = sinon.spy();

        handler.calculationDeduct(testData, '', callback);

        validation
            .validate(testData, '/Calculation')
            .then(() => {
                callback.should.have.been.calledWith(null, {
                    statusCode: 200,
                    body: JSON.stringify({
                        data: {
                            result: 1
                        }
                    })
                });

                done();
            })
            .catch(() => {
                done();
            });
    });

    it('calculationDeduct. Validation failed.', (done) => {
        const testData = {};
        const callback = sinon.spy();

        handler.calculationDeduct(testData, '', callback);

        validation
            .validate(testData, '/Calculation')
            .then(() => {
                done();
            })
            .catch((err) => {
                callback.should.have.been.calledWith(null, {
                    statusCode: 400,
                    body: JSON.stringify({
                        err
                    })
                });

                done();
            });
    });
});
