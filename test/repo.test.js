require('./load-env');
const repo = require('../repository/repo');
const chai = require('chai');

const { expect } = chai;

describe('repository/repo', () => {
    describe('newFunction', () => {
        it('Should return "new functions"', () => {
            expect(repo.newFunction()).to.equal('new functions');
        });
    });
});

