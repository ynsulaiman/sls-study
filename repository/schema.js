const dynamoose = require('./connector-dynamodb');

const { Schema } = dynamoose;

const config = JSON.parse(process.env.dynamoose);

const Calculation = new Schema(
  {
    CalculationId: {
      type: String,
      trim: true,
      hashKey: true
    },
    ValA: {
      type: Number
    },
    ValB: {
      type: Number
    },
    Type: {
      type: String
    },
    Result: {
      type: Number
    }
  },
  {
    throughput: {
      read: config.throughput.CalculationTable.read,
      write: config.throughput.CalculationTable.write
    },

    timestamps: {
      createdAt: 'CreatedAt',
      updatedAt: 'UpdatedAt'
    }
  }
);

module.exports = Calculation;
