const chai = require('chai');

const { expect } = chai;

describe('util/logging', () => {
    describe('log.level', () => {
        it('Should return the level if log is enabled.', () => {
            process.env = {};
            process.env.logging = JSON.stringify({
                enabled: true,
                level: 'info'
            });

            delete require.cache[require.resolve('../util/logging')];
            const logUtils = require('../util/logging');  // eslint-disable-line

            expect(logUtils.level).to.equal('info');
        });

        it('Should return "silent" if the log is NOT enabled.', () => {
            process.env = {};
            process.env.logging = JSON.stringify({
                enabled: false,
                level: 'info'
            });

            delete require.cache[require.resolve('../util/logging')];
            const logUtils = require('../util/logging');  // eslint-disable-line

            expect(logUtils.level).to.equal('silent');
        });
    });
});
