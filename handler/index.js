const calculationsLogic = require('../logic/calculation');
const paramsUtils = require('../util/params');
const responseUtils = require('../util/response');
const validation = require('../validation');
const log = require('../util/logging');
const calrepo = require('../repository/repo');


const calculationAdd = (event, context, callback) => {
    const body = paramsUtils.getBody(event);

    log.info(`body: ${JSON.stringify(body)})`);

    validation
        .validate(body, '/Calculation')
        .then(() => {
            const { valA, valB } = body;
            const result = calculationsLogic.calculationAdd(valA, valB);
            const response = responseUtils.successResponse({
                result,
                development: process.env.development,
                region: process.env.region
            });

            calrepo
            .save({
                CalculationId: uuid(),
                ValA: valA,
                ValB: valB,
                Type:'add',
                Result: result
            }, (saveErr)=>{
                if(saveErr){
                    console.log(`Error saving: ${saveErr} `);
                    return;
                }
            });

            callback(null, response);
        })
        .catch((err) => {
            const response = responseUtils.errorResponse(400, err);

            callback(null, response);
        });
};

const calculationDeduct = (event, context, callback) => {
    const query = paramsUtils.getQuery(event);
    const uuid = require('uuid/v4');

    validation
        .validate(query, '/Calculation')
        .then(() => {
            const { valA, valB } = query;
            const result = calculationsLogic.calculationDeduct(valA, valB);
            const response = responseUtils.successResponse({
                result,
                development: process.env.development,
                region: process.env.region
            });

            calrepo.save({
                CalculationId: uuid(),
                ValA: valA,
                ValB: valB,
                Type:'deduct',
                Result: result
            }, (saveErr)=>{
                if(saveErr){
                    console.log(`Error saving: ${saveErr} `);
                    return;
                }
            });

            callback(null, response);
        })
        .catch((err) => {
            const response = responseUtils.errorResponse(400, err);

            callback(null, response);
        });
};

const calculationGet = (event, context, callback) => {
    const body = paramsUtils.getBody(event);

    log.info(`body: ${JSON.stringify(body)})`);

    calrepo
        .get({ CalculationId: id })
        .then((result) =>{
            const response = responseUtils.successResponse({
                result,
                development: process.env.development,
                region: process.env.region
            });
        })
        .catch((err) => {
            const response = responseUtils.errorResponse(400, err);

            callback(null, response);
        });
}

const calculationList = (event, context, callback) => {
    
    calrepo.scan().limit(10).exec()
    .then((calculations) => {
        const response = responseUtils.successResponse({
            calculations,
            development: process.env.development,
            region: process.env.region
        });
    })
    .catch((err) => {
        const response = responseUtils.errorResponse(400, err);

        callback(null, response);
    });
};

const calculationDelete = (event, context, callback) => {
    const query = paramsUtils.getQuery(event);
    const { id } = query;

    calrepo
    .delete({ ValA: id })
    .then((result) => {
        const response = responseUtils.successResponse({
            status: 'Data deleted.',
            development: process.env.development,
            region: process.env.region
        });
    })
    .catch((err) => {
        const response = responseUtils.errorResponse(400, err);

        callback(null, response);
    });
};

const calculationUpdate = (event, context, callback) => {
    const body = paramsUtils.getBody(event);

    log.info(`body: ${JSON.stringify(body)})`);
    const { id, valA, valB, type } = body;

    calrepo
    .get({ CalculationId: id })
    .then((getResult) =>{

        if (type == 'deduct'){
            const result = calculationsLogic.calculationAdd(valA, valB);
        }
        else{
            const result = calculationsLogic.calculationAdd(valA, valB);
        }

        calrepo
        .update(
            {
                CalculationId: id
            },
            {
                ValA: valA,
                ValB: valB,
                Result: result,
                Type: type
            }
        )
        .then((result) => {
            const response = responseUtils.successResponse({
                status: 'Data updated.',
                development: process.env.development,
                region: process.env.region
            });
        })
        .catch((updateErr) => {
            const response = responseUtils.errorResponse(400, updateErr);
    
            callback(null, response);
        });
    })
    .catch((err) => {
        const response = responseUtils.errorResponse(400, err);

        callback(null, response);
    });

    
};

module.exports = {
    calculationAdd,
    calculationDeduct,
    calculationGet,
    calculationList,
    calculationUpdate,
    calculationDelete

};
