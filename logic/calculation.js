/* eslint class-methods-use-this:[off] */
const log = require('../util/logging');

class Test {
    calculationAdd(valA, valB) {
        log.debug(`typeof valA: ${typeof valA} and typeof valB: ${typeof valB}`);
        return valA + valB;
    }

    calculationDeduct(valA, valB) {
        log.debug(`typeof valA: ${valA} and typeof valB: ${valB}`);
        return valA - valB;
    }
}

module.exports = new Test();
