const pino = require('pino');

const pretty = pino.pretty();
const loggingConfigString = (process.env && process.env.logging) || '{"enabled":true,"level":"debug"}';
const loggingConfig = JSON.parse(loggingConfigString);

pretty.pipe(process.stdout);

const log = pino({
    name: process.env.serviceName,
    safe: true,
    levelFirst: true
}, pretty);

if (loggingConfig.enabled === true) {
    log.level = loggingConfig.level;
} else {
    log.level = 'silent';
}

module.exports = log;
