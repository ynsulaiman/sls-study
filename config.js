const _ = require('lodash');

const defaultValue = {
    serviceName: 'Structure Service',
    logging: JSON.stringify({
        enabled: true,
        level: 'debug'
    }),
    dynamoose: JSON.stringify({
        create: true,
        update: true,
        local: true,
        throughput: {
            CalculationTable: {
                read: 1,
                write: 1
            }
        }
    }),
    accountId: '128449538950',
    stage: 'dev',
    region: 'us-west-2',
    deploymentBucket: 'wallex-serverless-deployments-oregon'
};

const dev = () =>
    _.extend(defaultValue, {

    });

const yasmine = () =>
    _.extend(defaultValue, {
        deploymentBucket: 'yasmine-serverless-deployments-oregon'
    });

const staging = () =>
    _.extend(defaultValue, {
        logging: JSON.stringify({
            enabled: true,
            level: 'debug'
        }),
        dynamoose: JSON.stringify({
            create: false,
            local: false
        }),
        stage: 'staging',
        region: 'ap-south-1',
        deploymentBucket: 'wallex-serverless-deployments-mumbai'
    });

const demo = () =>
    _.extend(defaultValue, {
        logging: JSON.stringify({
            enabled: true,
            level: 'debug'
        }),
        dynamoose: JSON.stringify({
            create: false,
            local: false
        }),
        stage: 'demo',
        region: 'ap-northeast-1',
        deploymentBucket: 'wallex-serverless-deployments-tokyo'
    });

const prod = () =>
    _.extend(defaultValue, {
        logging: JSON.stringify({
            enabled: true,
            level: 'info'
        }),
        dynamoose: JSON.stringify({
            create: false,
            local: false
        }),
        stage: 'prod',
        region: 'ap-southeast-1',
        deploymentBucket: 'wallex-serverless-deployments'
    });

module.exports = {
    dev,
    yasmine,
    staging,
    demo,
    prod
};
