module.exports = {
    id: '/Calculation',
    type: 'object',
    properties: {
        valA: { type: 'number' },
        valB: { type: 'number' }
    },
    required: ['valA', 'valB']
};
