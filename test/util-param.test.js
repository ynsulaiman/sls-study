const params = require('../util/params');
const logUtils = require('../util/logging');
const chai = require('chai');
const sinon = require('sinon');

const { expect } = chai;

describe('util/params', () => {
    let info;
    let error;

    describe('getBody from event', () => {
        beforeEach(() => {
            // Skip any log output
            info = sinon.stub(logUtils, 'info');
            error = sinon.stub(logUtils, 'error');
        });

        afterEach(() => {
            info.restore();
            error.restore();
        });

        it('Should return object from event', () => {
            expect(params.getBody({ valA: 1, valB: 2 }))
                .to.deep.equal({ valA: 1, valB: 2 });
        });

        it('Should return empty object from event, if undefined is provided', () => {
            expect(params.getBody(undefined))
                .to.deep.equal({});
        });

        it('Should return empty object from event, if try catch happen.', () => {
            expect(params.getBody('Some string that cannot parsed.'))
                .to.deep.equal({});
        });
    });

    describe('getBody from http', () => {
        beforeEach(() => {
            // Skip any log output
            info = sinon.stub(logUtils, 'info');
            error = sinon.stub(logUtils, 'error');
        });

        afterEach(() => {
            info.restore();
            error.restore();
        });

        it('Should return object from http', () => {
            expect(params.getBody({ body: JSON.stringify({ valA: 1, valB: 2 }) }))
                .to.deep.equal({ valA: 1, valB: 2 });
        });

        it('Should return exact object from http, if not string.', () => {
            expect(params.getBody({ body: { valA: 1, valB: 2 } }))
                .to.deep.equal({ valA: 1, valB: 2 });
        });

        it('Should return empty object from http, if undefined is provided.', () => {
            expect(params.getBody({ body: undefined }))
                .to.deep.equal({});
        });

        it('Should return empty object from http, if try catch happen.', () => {
            expect(params.getBody({ body: 'Some string that cannot parsed.' }))
                .to.deep.equal({});
        });
    });

    describe('getquery from http', () => {
        beforeEach(() => {
            // Skip any log output
            info = sinon.stub(logUtils, 'info');
            error = sinon.stub(logUtils, 'error');
        });

        afterEach(() => {
            info.restore();
            error.restore();
        });

        it('Should return object from http query url', () => {
            expect(params.getQuery({ queryStringParameters: { valA: 1, valB: 2 } }))
                .to.deep.equal({ valA: 1, valB: 2 });
        });
    });
});

